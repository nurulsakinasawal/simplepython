import cv2
import numpy as np

img = np.zeros((512, 512, 3), np.uint8)
# img = np.zeros((512,512,3))
print(img)

# img[:] = 255,0,0
# line img
cv2.line(img, (0, 0), (img.shape[1], img.shape[0]), (0, 255, 0), 2)

# create rectangle line
# cv2.rectangle(img,(350,100),(450,200),(0,0,255),2)
# filled rectangle
cv2.rectangle(img, (350, 100), (450, 200), (0, 0, 255), cv2.FILLED)
# create circle
cv2.circle(img, (150, 400), 50, (255, 0, 0), 3)
# put text
cv2.putText(img,"Draw Shape",(75,50),cv2.FONT_HERSHEY_COMPLEX,1,(0,150,0),1)

cv2.imshow("image", img)

cv2.waitKey(0)
